package com.bubladecoding.mcpluginbase.data;

/**
 * Created by: Alex ter Horst
 * Project: McPluginBase
 * Creation date: 26/11/2017
 */
public enum DatabaseType {

    MYSQL("mysql"), SQLITE("sqlite");

    private String name;

    DatabaseType(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static DatabaseType getByName(String name){
        for (DatabaseType dbt : DatabaseType.values())
            if (dbt.getName().equalsIgnoreCase(name))
                return dbt;
        return MYSQL;
    }
}
