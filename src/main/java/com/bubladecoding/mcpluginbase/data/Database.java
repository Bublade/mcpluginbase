package com.bubladecoding.mcpluginbase.data;

import com.bubladecoding.mcpluginbase.McPluginBase;
import com.bubladecoding.mcpluginbase.configuration.Config;
import com.bubladecoding.mcpluginbase.configuration.Configs;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by: Alex ter Horst
 * Project: McPluginBase
 * Creation date: 26/11/2017
 */
public class Database extends HashMap<String, Connection> {

    private static Database instance;
    private Config databases;

    private Database() {
        databases = Configs.databases;
        Set<String> dbs = databases.getConfig().getKeys(false);
        dbs.forEach((key) -> {
            if (databases.getConfig().getBoolean(key + ".autoconnect"))
                connect(key);
        });
    }

    public static void setInstance() {
        if (instance != null)
            throw new IllegalStateException("Only one instance can run");
        instance = new Database();
    }

    public static Database getInstance() {
        if (instance == null)
            throw new IllegalStateException("Cannot get MySqlDatabase instance: instance is null did you forget to set the instance?");
        return instance;
    }

    /**
     * Get the connection with the given name
     * @param name the name of the connection  you want.
     * @return the connection that was found.
     */
    public static Connection getConnection(String name) {
        if (!instance.containsKey(name))
            throw new NullPointerException("Database " + name + " does not exist!");
        if (instance.get(name) == null)
            throw new NullPointerException("Database " + name + " has no connection!");
        return instance.get(name);
    }

    /**
     * Connect with the database with the given name.
     * @param name the name of the database you want to connect to.
     */
    public void connect(String name) {
        try {
            FileConfiguration fc = databases.getConfig();
            DatabaseType type = DatabaseType.getByName(fc.getString(name + ".type"));
            switch (type) {
                case MYSQL:
                    connectMySql(name);
                    break;
                case SQLITE:
                    connectSqLite(name);
                    break;
                default:
                    this.put(name, null);
                    break;
            }
        } catch (SQLException | IOException | ClassNotFoundException ex) {
            this.put(name, null);
            ex.printStackTrace();
        }
    }

    private void connectMySql(String name) throws SQLException {
        FileConfiguration fc = databases.getConfig();
        String host = fc.getString(name + ".host");
        String port = fc.getString(name + ".port");
        String dbName = fc.getString(name + ".name");
        String user = fc.getString(name + ".user");
        String password = fc.getString(name + ".password");
        this.put(name, DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + dbName, user, password));
    }

    private void connectSqLite(String name) throws IOException, ClassNotFoundException, SQLException {
        String filename = databases.getConfig().getString(name + ".filename");
        File datafile = new File(McPluginBase.getInstance().getDataFolder(), name + ".db");
        if (!datafile.exists()) {
            if (!datafile.getParentFile().exists())
                datafile.getParentFile().mkdirs();
            datafile.createNewFile();
        }
        Class.forName("org.sqlite.JDBC");
        this.put(name, DriverManager.getConnection("jdbc:sqlite:" + datafile));
    }


}
