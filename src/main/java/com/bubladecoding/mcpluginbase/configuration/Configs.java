package com.bubladecoding.mcpluginbase.configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by: Alex ter Horst
 * Project: McPluginBase
 * Creation date: 25/11/2017
 */
public final class Configs extends HashMap<String, Config> {

    private static Configs instance;

    public static Configs getInstance() {
        if (instance == null)
            instance = new Configs();
        return instance;
    }

    private Configs(){
        messages = put("messages", new Config("messages"));
        config = put("config", new Config("config"));
        databases = put("databases", new Config("databases"));
    }

    public static Config messages;
    public static Config config;
    public static Config databases;

    public static void saveDefaultConfigs(boolean override){
        if (instance == null)
            getInstance();
        for (Map.Entry<String, Config> conf : instance.entrySet()) {
            conf.getValue().saveDefaultConfig(override);
            conf.getValue().saveConfig();
        }
    }

    /**
     * Get a config with the given key (name of the config)
     * @param key the name of the config you want to get
     * @return the config
     */
    public Config get(String key) {
        return super.get(key.toLowerCase());
    }

    /**
     * add a config
     * @param key the key/name of the config
     * @param c the config that goes with the given key
     * @return the added config
     */
    @Override
    public Config put(String key, Config c){
        super.put(key.toLowerCase(), c);
        return c;
    }


}
