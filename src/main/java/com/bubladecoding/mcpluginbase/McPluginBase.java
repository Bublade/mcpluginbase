package com.bubladecoding.mcpluginbase;

import com.bubladecoding.mcpluginbase.command.CommandManager;
import com.bubladecoding.mcpluginbase.configuration.Configs;
import com.bubladecoding.mcpluginbase.data.Database;
import com.bubladecoding.mcpluginbase.event.EventManager;
import com.bubladecoding.mcpluginbase.utils.ChatUtils;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public final class McPluginBase extends JavaPlugin {

    private static McPluginBase instance = null;

    public McPluginBase() throws IllegalStateException {
        if (instance != null)
            throw new IllegalStateException("Only one instance can run");
        instance = this;
    }

    public static McPluginBase getInstance() {
        if (instance == null)
            throw new IllegalStateException("Cannot get instance: instance is null");
        return instance;
    }

    @Override
    public void onEnable() {
        Configs.saveDefaultConfigs(false);
        CommandManager.registerAll(this);
        EventManager.registerAll(this);
        /*
         * DO NOT REGISTER COMMANDS OR EVENTS HERE!
         *
         * Commands should go in the CommandManager! Listeners should go in the
         * EventManager!
         */

        // to make sure the files are created before the connect method tries to use it.
        // Three second delay
        // Add and enable databases in the database.yml file
        Bukkit.getScheduler().runTaskLater(this, Database::setInstance, 60);
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
