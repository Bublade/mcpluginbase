package com.bubladecoding.mcpluginbase.utils;

import org.bukkit.ChatColor;

import java.util.Locale;

public final class ChatUtils {

    /**
     * Get a formatted string using {@link ChatColor#translateAlternateColorCodes}
     *
     * @param format The string to format and translate the colors of.
     * @return the formatted and colored string.
     * @see org.bukkit.ChatColor#translateAlternateColorCodes(char, String)
     */
    public static String format(String format) {
        return ChatColor.translateAlternateColorCodes('&', format);
    }

    /**
     * Get a formatted string using {@link ChatColor#translateAlternateColorCodes} and {@link String#format}
     *
     * @param format The string to format and translate the colors of.
     * @param args   Optional arguments supplied to {@link String#format(String, Object...)}
     * @return the formatted and colored string.
     * @see org.bukkit.ChatColor#translateAlternateColorCodes(char, String)
     * @see java.lang.String#format(Locale, String, Object...)
     */
    public static String format(String format, Object... args) {
        return format(null, format, args);
    }

    /**
     * Get a formatted string using {@link ChatColor#translateAlternateColorCodes} and {@link String#format}
     *
     * @param locale The {@linkplain java.util.Locale locale} to apply during formatting.
     * @param format The string to format and translate the colors of.
     * @param args   Optional arguments supplied to {@link String#format(String, Object...)}
     * @return the formatted and colored string.
     * @see java.util.Locale locale
     * @see org.bukkit.ChatColor#translateAlternateColorCodes(char, String)
     * @see java.lang.String#format(Locale, String, Object...)
     */
    public static String format(Locale locale, String format, Object... args) {
        return ChatColor.translateAlternateColorCodes('&', String.format(locale, format, args));
    }
}
