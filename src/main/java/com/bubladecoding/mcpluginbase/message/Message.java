package com.bubladecoding.mcpluginbase.message;

import com.bubladecoding.mcpluginbase.configuration.Configs;
import com.bubladecoding.mcpluginbase.utils.ChatUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by: Alex ter Horst
 * Project: McPluginBase
 * Creation date: 25/11/2017
 */

public enum Message {

    /* messages.yml > errors.error_occurred */
    ERROR_OCCURRED("errors"),

    /* messages.yml > errors.invalid_command */
    INVALID_COMMAND("errors"),

    /* messages.yml > errors.to_few_arguments */
    TO_FEW_ARGUMENTS("errors"),

    /* messages.yml > authorization.no_permission */
    NO_PERMISSION("authorization"),

    /* messages.yml > message.welcome.welcome_title */
    WELCOME_TITLE("message.welcome"),

    /* messages.yml > message.welcome.welcome_subtitle */
    WELCOME_SUBTITLE("message.welcome"),

    /* messages.yml > messages.player_joined */
    PLAYER_JOINED(),

    /* messages.yml > messages.player_quit */
    PLAYER_QUIT(),

    /* message.yml > administration.configs_reloaded */
    CONFIGS_RELOADED("administration"),

    /* message.yml > administration.configs_not_reloaded */
    CONFIGS_NOT_RELOADED("administration"),

    /* message.yml > administration.config_reloaded */
    CONFIG_RELOADED("administration"),

    /* message.yml > administration.config_not_reloaded */
    CONFIG_NOT_RELOADED("administration"),

    /* message.yml > administration.config_does_not_exist */
    CONFIG_DOES_NOT_EXIST("administration");

    /**
     * Location of the message
     */
    private String messageLocation;

    /**
     * Constructor the message will be added to the "message." location by default
     */
    Message() {
        this.messageLocation = "message";
    }

    /**
     * Constructor the message will be added to the "message." location by default
     *
     * @param messageLocation the parent location of the message
     */
    Message(String messageLocation) {
        this.messageLocation = messageLocation;
    }

    /**
     * Get the string from the messages.yml related to the selected message
     *
     * @return the string from the config (color codes will be applied automatically '&' char )
     */
    public String getString() {
        FileConfiguration messages = Configs.messages.getConfig();
        String location = messageLocation + "." + toString().toLowerCase();
        if (messages.getString(location) == null)
            return ChatColor.translateAlternateColorCodes('&', "&cNULL: " + location);
        return ChatColor.translateAlternateColorCodes('&', messages.getString(location));
    }


    /**
     * Get the string from the messages.yml related to the selected message
     *
     * @param replaceable the values for the place holders.
     * @return the string from the config (color codes will be applied automatically '&' char )
     */
    public String getString(Replaceable replaceable) {
        String msg = getString();

        for (Map.Entry<String, Object> k : replaceable.entrySet()) {
            msg = msg.replaceAll(Pattern.quote(k.getKey()), k.getValue().toString());
        }

        return ChatUtils.format(msg);
    }


    /**
     * Send the current message to a command sender
     *
     * @param sender the sender you want to send it to
     */
    public void send(CommandSender sender) {
        sender.sendMessage(this.getString());
    }


    /**
     * Send the current message to a command sender.
     *
     * @param sender      the sender you want to send it to.
     * @param replaceable the values for the place holders.
     */
    public void send(CommandSender sender, Replaceable replaceable) {
        sender.sendMessage(getString(replaceable));
    }


}
