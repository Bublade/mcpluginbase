package com.bubladecoding.mcpluginbase.message;

import java.util.HashMap;

/**
 * Created by: Alex ter Horst
 * Project: McPluginBase
 * Creation date: 02/02/2018
 */
public class Replaceable extends HashMap<String, Object> {
    public Replaceable() {
    }

    public Replaceable(String old, Object o) {
        this.put(old, o);
    }

    public Replaceable add(String old, Object o) {
        this.put(old, o);
        return this;
    }
}
