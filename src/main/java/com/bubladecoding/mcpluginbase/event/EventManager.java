package com.bubladecoding.mcpluginbase.event;

import com.bubladecoding.mcpluginbase.event.listeners.PlayerEvents;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

/**
 * Created by: Alex ter Horst
 * Project: McPluginBase
 * Creation date: 25/11/2017
 */
public class EventManager {

    private static EventManager manager;
    private ArrayList<Listener> listeners = new ArrayList<>();

    /**
     * private constructor
     */
    private EventManager() {

        //ADD EVENT LISTENERS HERE
        listeners.add(new PlayerEvents());

        // keep this as last!
        manager = this;
    }

    /**
     * register all the Listeners set in the constructor
     *
     * @param plugin the plugin
     */
    public static void registerAll(JavaPlugin plugin) {
        if (manager == null)
            manager = new EventManager();
        PluginManager pm = Bukkit.getPluginManager();
        manager.listeners.forEach((l) -> pm.registerEvents(l, plugin));
    }
}
