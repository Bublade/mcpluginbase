package com.bubladecoding.mcpluginbase.event.listeners;

import com.bubladecoding.mcpluginbase.message.Message;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by: Alex ter Horst
 * Project: McPluginBase
 * Creation date: 25/11/2017
 */
public class PlayerEvents implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e){
        e.setJoinMessage(Message.PLAYER_JOINED.getString().replace("{PLAYER}", e.getPlayer().getDisplayName()));
        if (!e.getPlayer().hasPlayedBefore()) {
            e.getPlayer().sendTitle(Message.WELCOME_TITLE.getString(), Message.WELCOME_SUBTITLE.getString(), 10, 70, 20);
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e){
        e.setQuitMessage(Message.PLAYER_QUIT.getString().replace("{PLAYER}", e.getPlayer().getDisplayName()));
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerChat(AsyncPlayerChatEvent e){
        String message = e.getMessage();
        Set<Player> players = Arrays.stream(message.split(" ")).map(Bukkit::getPlayerExact).filter(Objects::nonNull).filter(e.getRecipients()::contains).collect(Collectors.toSet());
        e.getRecipients().removeAll(players);
        players.forEach(p -> {
            p.sendMessage(message.replace(p.getName(), ChatColor.UNDERLINE + p.getName()));
            p.playSound(p.getLocation(), Sound.ENTITY_ITEM_PICKUP, 1, 1);
        });
    }


}
