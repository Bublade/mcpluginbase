package com.bubladecoding.mcpluginbase.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by: Alex ter Horst
 * Project: McPluginBase
 * Creation date: 25/11/2017
 */
public abstract class ArgumentCommand {

    /**
     * Name of the sub command
     */
    private String name;

    /**
     * The args definition (order)
     */
    private String args;

    /**
     * The permission needed for this sub command
     */
    private String permission;

    /**
     * If false the given arguments (in the constructor will not be check if valid)
     */
    private boolean checkValidArguments;

    /**
     * Constructor
     *
     * @param name Name of the sub command
     * @param args The args definition (order)
     */
    public ArgumentCommand(String name, String args) {
        this(name, args, true);
    }

    /**
     * @param name                Name of the sub command
     * @param args                The args definition (order)
     * @param checkValidArguments Check the arguments for correct format
     */
    public ArgumentCommand(String name, String args, boolean checkValidArguments) {
        this.name = name;
        this.args = args;
        this.checkValidArguments = checkValidArguments;
    }

    /**
     * Constructor
     *
     * @param name       Name of the sub command
     * @param args       The args definition (order)
     * @param permission The permission needed for this sub command (null if no permission is needed!)
     */
    public ArgumentCommand(String name, String args, String permission) {
        this(name, args, permission, true);
    }

    /**
     * @param name                Name of the sub command
     * @param args                The args definition (order)
     * @param permission          The permission needed for this sub command (null if no permission is needed!)
     * @param checkValidArguments Check the arguments for correct format
     */
    public ArgumentCommand(String name, String args, String permission, boolean checkValidArguments) {
        this.name = name;
        this.args = args;
        this.permission = permission;
        this.checkValidArguments = checkValidArguments;
    }

    /**
     * Get the name of the sub command
     *
     * @return the name of the sub command
     */
    public String getName() {
        return name;
    }

    /**
     * Get the arguments of this sub command
     *
     * @return the arguments of this sub command
     */
    public String getArgs() {
        return args;
    }

    /**
     * Run the sub command without giving the cmd
     *
     * @param sender the sender of the command executed
     * @param args   the arguments of the command executed
     */
    public void run(CommandSender sender, String[] args) {
    }

    /**
     * Run the sub command without giving the cmd
     *
     * @param cmd    the main command that was executed
     * @param sender the sender of the command executed
     * @param args   the arguments of the command executed
     */
    public void run(Command cmd, CommandSender sender, String[] args) {
    }

    /**
     * Check if the sender has the permissions for this command
     *
     * @param sender the sender of the command
     * @return whether the sender has te needed permission or true if the permission is null
     */
    public boolean allowRun(CommandSender sender) {
        return (permission == null || sender.hasPermission(permission));
    }

    /**
     * check if the arguments are correct ( and valid if enabled )
     *
     * @param x the arguments
     * @return whether the player have given enough arguments and whether the arguments are correct
     */
    public boolean enoughArguments(String[] x) {
        String[] argus = args.split(" ");
        int i = argus.length;
        if (checkValidArguments) {
            i = 0;
            boolean closed = false;
            for (String s : argus) {
                if (s.startsWith("<") && s.endsWith(">")) {
                    if (closed) {
                        throw new InvalidParameterException("Invalid argument command \"" + name + "\" @" + this.toString());
                    } else
                        i++;
                }
                if ((s.startsWith("[") && s.endsWith("]")) || (s.startsWith("(") && s.endsWith(")")))
                    closed = true;
            }
        }
        return i <= x.length;
    }

     /**
     * The tab completion handler for the argument commands
     *
     * @param sender the sender that executed the command
     * @param cmd    the command executed
     * @param alias  the aliases for that command
     * @param args   the arguments so far
     * @return the list of options
     */
    public List<String> onTabComplete(final CommandSender sender, final Command cmd, final String alias, final String[] args) {
        return new ArrayList<>();
    }

    public String getUsage(String mainCmd) {
        return mainCmd + " " + getName() + " " + getArgs();
    }

}
