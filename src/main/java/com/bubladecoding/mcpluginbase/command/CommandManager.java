package com.bubladecoding.mcpluginbase.command;

import com.bubladecoding.mcpluginbase.command.admin.ReloadConfig;
import com.bubladecoding.mcpluginbase.message.Message;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;

/**
 * Created by: Alex ter Horst Project: McPluginBase Creation date: 25/11/2017
 */
public class CommandManager implements CommandExecutor, TabCompleter {

    private static CommandManager manager;
    private HashMap<MainCommand, List<ArgumentCommand>> cmds;
    private HashMap<String, CommandExecutor> defaultCmds;
    private HashMap<String, TabCompleter> defaultCmdsTabCompleter;

    /**
     * Constructor Add your commands and sub commands in here!
     */
    private CommandManager() {
        defaultCmds = new HashMap<>();
        defaultCmdsTabCompleter = new HashMap<>();
        cmds = new HashMap<>();

        // commands and argument commands here!
        /*
         * registerArgumentCommands(new MainCommand("something"), new SubCommand1(), new
         * SubCommand2() , new SubCommand3(), etc..... )
         */

        registerDefaultCommand("reloadconfig", new ReloadConfig());

    }

    /**
     * Register all the commands that are registered in this calls
     *
     * @param plugin
     */
    public static void registerAll(JavaPlugin plugin) {
        if (manager == null)
            manager = new CommandManager();

        if (manager.cmds.size() > 0)
            manager.cmds.forEach((key, value) -> {
                plugin.getCommand(key.getName()).setExecutor(manager);
                plugin.getCommand(key.getName()).setTabCompleter(manager);
            });

        if (manager.defaultCmds.size() > 0)
            manager.defaultCmds.forEach((key, value) -> plugin.getCommand(key).setExecutor(value));

        if (manager.defaultCmdsTabCompleter.size() > 0)
            manager.defaultCmdsTabCompleter.forEach((key, value) -> plugin.getCommand(key).setTabCompleter(value));
        /*
         * add extra things to commands here (like tabComplete) for normal commands if
         * you only have a executor set it in the constructor instead
         */
    }

    /**
     * Register a list of argument commands all at one
     *
     * @param cmd             the main command that you want to give an argument
     *                        commands
     * @param argumentCommand a single or multiple argument commands
     */
    private void registerArgumentCommands(MainCommand cmd, ArgumentCommand... argumentCommand) {
        for (ArgumentCommand acmd : argumentCommand)
            registerArgumentCommand(cmd, acmd);
    }

    /**
     * Register a list of argument commands all at one
     *
     * @param cmd             the main command that you want to give an argument
     *                        commands
     * @param argumentCommand a single argument command
     */
    private void registerArgumentCommand(MainCommand cmd, ArgumentCommand argumentCommand) {
        if (cmds.containsKey(cmd))
            cmds.get(cmd).add(argumentCommand);
        else {
            ArrayList<ArgumentCommand> bcs = new ArrayList<>();
            bcs.add(argumentCommand);
            cmds.put(cmd, bcs);
        }
    }

    /**
     * @param cmd    the cmd name
     * @param cmdexe the executor
     */
    private void registerDefaultCommand(String cmd, CommandExecutor cmdexe) {
        if (!defaultCmds.containsKey(cmd))
            defaultCmds.put(cmd, cmdexe);
    }

    /**
     * The onCommand function that runs all argument commands
     *
     * @param sender       the sender of the command
     * @param cmd          the executed command
     * @param commandLabel the label of the command
     * @param args         the arguments that the player typed
     * @return if false show usage dialog set in the plugin.yml
     */
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        MainCommand mc = null;
        List<ArgumentCommand> subCmds = new ArrayList<>();
        for (Map.Entry<MainCommand, List<ArgumentCommand>> aCmd : cmds.entrySet()) {
            if (aCmd.getKey().getName().equalsIgnoreCase(cmd.getName())) {
                mc = aCmd.getKey();
                subCmds = aCmd.getValue();
                break;
            }
        }

        if (mc == null) {
            Message.INVALID_COMMAND.send(sender);
            return true;
        }

        if (args.length == 0) {
            if (!mc.customHelp(sender)) {
                subCmds.forEach((c) -> sender.sendMessage(c.getUsage(cmd.getName())));
            }
            return true;
        }

        if (mc.run(sender, cmd, args)) {
            ArrayList<String> a = new ArrayList<>(Arrays.asList(args));
            a.remove(0);
            String[] newArgs = a.toArray(new String[0]);
            for (ArgumentCommand c : subCmds)
                if (c.getName().equalsIgnoreCase(args[0])) {
                    try {
                        if (c.allowRun(sender)) {
                            if (c.enoughArguments(newArgs)) {
                                c.run(sender, newArgs);
                                c.run(cmd, sender, newArgs);
                            } else
                                Message.TO_FEW_ARGUMENTS.send(sender);
                        } else
                            Message.NO_PERMISSION.send(sender);
                    } catch (Exception e) {
                        Message.ERROR_OCCURRED.send(sender);
                        e.printStackTrace();
                        return false;
                    }
                    return true;
                }
        }
        Message.INVALID_COMMAND.send(sender);
        return true;
    }

    /**
     * The tab completion handler for the argument commands
     *
     * @param sender the sender that executed the command
     * @param cmd    the command executed
     * @param alias  the aliases for that command
     * @param args   the arguments so far
     * @return the list of options
     */
    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String alias, String[] args) {
        List<String> subCmds = new ArrayList<>();

        for (Map.Entry<MainCommand, List<ArgumentCommand>> set : cmds.entrySet()) {
            MainCommand key = set.getKey();
            List<ArgumentCommand> value = set.getValue();
            if (key.getName().equalsIgnoreCase(cmd.getName())) {
                if (args.length <= 1) {
                    value.forEach((argumentCommand) -> subCmds.add(argumentCommand.getName()));
                } else {
                    for (ArgumentCommand argumentCommand : value)
                        if (argumentCommand.getName().equalsIgnoreCase(args[0])) {
                            subCmds.addAll(argumentCommand.onTabComplete(sender, cmd, alias, args));
                            break;
                        }
                }
                break;
            }

        }
        return subCmds;
    }

}
