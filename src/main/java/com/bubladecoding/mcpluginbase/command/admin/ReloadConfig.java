package com.bubladecoding.mcpluginbase.command.admin;

import com.bubladecoding.mcpluginbase.configuration.Config;
import com.bubladecoding.mcpluginbase.configuration.Configs;
import com.bubladecoding.mcpluginbase.message.Message;
import com.bubladecoding.mcpluginbase.message.Replaceable;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 * Created by: Alex ter Horst
 * Project: McPluginBase
 * Creation date: 29/11/2017
 */
public class ReloadConfig implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender.hasPermission(command.getPermission())) {
            if (args.length == 0) {
                try {
                    Configs.getInstance().forEach((k, v) -> v.reloadConfig());
                    Message.CONFIGS_RELOADED.send(sender);
                } catch (Exception e) {
                    Message.CONFIGS_NOT_RELOADED.send(sender);
                }
            } else {
                for (String s : args) {
                    Config c = Configs.getInstance().get(s);
                    Replaceable rpl = new Replaceable("{CONFIG}", s);
                    try {
                        if (c != null) {
                            c.reloadConfig();
                            Message.CONFIG_RELOADED.send(sender, rpl);
                        } else {
                            Message.CONFIG_DOES_NOT_EXIST.send(sender, rpl);
                        }
                    } catch (Exception e) {
                        Message.CONFIG_NOT_RELOADED.send(sender, rpl);
                    }
                }
            }
        } else
            Message.NO_PERMISSION.send(sender);
        return true;
    }
}
