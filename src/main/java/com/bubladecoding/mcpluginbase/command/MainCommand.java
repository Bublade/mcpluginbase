package com.bubladecoding.mcpluginbase.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/**
 * Created by: Alex ter Horst
 * Project: McPluginBase
 * Creation date: 25/11/2017
 */
public class MainCommand {

    private String name;

    /**
     * Constructor
     *
     * @param name the name for the command
     */
    public MainCommand(String name) {
        this.name = name;
    }

    /**
     * Get the name of the command
     *
     * @return the name of the command
     */
    public String getName() {
        return name;
    }

    /**
     * toString function that returns the name of the command
     *
     * @return the name of the command
     */
    @Override
    public String toString() {
        return name;
    }

    /**
     * For when you want to have something in the main command
     *
     * @param sender the sender of the command (player, console or command block)
     * @param cmd    the command executed
     * @param args   the arguments that were given
     * @return whether the command was successful (if false the sub command will not be executed)
     */
    public boolean run(CommandSender sender, Command cmd, String[] args){
        return true;
    }

    /**
     * Send a custom help to a sender
     * @param receiver the one to receive the message
     * @return if it should be send (false if not overridden)
     */
    public boolean customHelp(CommandSender receiver) {
        return false;
    }


}
